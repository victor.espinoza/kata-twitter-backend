const {Pool} = require('pg');

export default class SqlQuery {
    db: any;

    constructor(config:Object) {
        this.db = new Pool(config);
    }

    async createDb(text:String, values:String[]) :Promise<Object[]>{
        const query = {
            text: text,
            values: values ? values : undefined
        }
        return await this._executeQuery(query);
    }

    select(queryText:String, values:String[]) :Promise<Object[]>{
        const query = {
            text: queryText,
            values: values ? values : undefined
        }
        return this._executeQuery(query).then(res=>Promise.resolve(res.rows));
    }

    selectFirst(queryText:String, values:String[]) :Promise<Object|null>{
        const query = {
            text: queryText,
            values: values,
        }
        return this._executeQuery(query)
            .then((res)=>Promise.resolve(res.rows && res.rows.length>0 ? res.rows[0] : null));
    }

    async insert(queryText: String, values: String[]) {
        const query = {
            text: queryText,
            values: values ? values : undefined
        }
        return await this._executeQuery(query);
    }

    async update(table: String, values: String[]){
        const query = {
            text: 'UPDATE ' + table + ' set "name"= $1 WHERE nick = $2',
            values: values,
        }
        return await this._executeQuery(query);
    }

    async deleteAll(table: String) {
        const query = {
            text: 'DELETE FROM ' + table
        }
        return await this._executeQuery(query);
    }

    private async _executeQuery(query: { text: String; values?: String[] }) {
        const client = await this.db.connect();
        let response: any;
        try {
            await client.query('BEGIN');
            response = await client.query(query);
            await client.query('COMMIT')
        } catch (e) {
            response = await client.query('ROLLBACK')
            console.log(e);
        } finally {
            client.end();
            return Promise.resolve(response);
        }
    }
}