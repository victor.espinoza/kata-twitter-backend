import UserService from "../domain/user/UserService";
import User from "../domain/user/User";
import UserBuilder from "../domain/user/UserBuilder";

export default class UserApi{
    private userService:UserService;

    constructor(userService:UserService) {
        this.userService = userService;
    }

    async register(request: any, res: any) {
        let user: User = new UserBuilder().withNick(request.body.nick).withName(request.body.name).build();
        await this.userService.register(user)
            .then(() => this.responseStatusAndMessage(res,200,'ok'))
            .catch(reason => this.responseStatusAndMessage(res,400,reason));
    }


    async updateName(request: any, response: any) {
        let user: User = new UserBuilder().withNick(request.body.nick).withName(request.body.name).build();
        await this.userService.updateName(user)
            .then(() => this.responseStatusAndMessage(response,200,'ok'))
            .catch(reason => this.responseStatusAndMessage(response,400,reason));

    }
    private responseStatusAndMessage(res:any, status:number, message:String) {
        res.status(status);
        res.send(message);
    }
}



