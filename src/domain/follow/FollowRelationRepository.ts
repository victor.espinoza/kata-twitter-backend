import FollowRelation from "./FollowRelation";

export default interface FollowRelationRepository{

    save(followRelation:FollowRelation):Promise<any>;

    findAllByNick(nick: String): Promise<FollowRelation[]>;

    findAll(): Promise<FollowRelation[]>;

    deleteAll():Promise<any>;
}