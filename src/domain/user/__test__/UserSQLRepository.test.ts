import User from "../User";
import UserBuilder from "../UserBuilder";
import UserSQLRepository from "../UserSQLRepository";
import SqlQuery from "../../../infraestructure/database/SqlQuery";
import {configTest} from "../../../infraestructure/database/Config";

describe.skip('UserSQLRepository',()=>{
    let userSQLRepository:UserSQLRepository;
    let userBuilder:UserBuilder;
    let user:User;
    let sqlQuery:SqlQuery;

    beforeEach(()=>{
        userBuilder = new UserBuilder();
        user = userBuilder.withName('name').withNick('nick').build();
        sqlQuery = new SqlQuery(configTest);
        userSQLRepository = new UserSQLRepository(sqlQuery);
    })

    afterEach(async ()=>{
        await userSQLRepository.deleteAll();
    })

    it('should find a saved user',async ()=>{
        user = userBuilder.withName('name3').withNick('nick3').build();
        await userSQLRepository.save(user);
        await userSQLRepository.findByNick(user.getNick()).then(res =>{
            expect(res).toStrictEqual(user);
        });
    })

    it('should save expected number of users',async ()=>{
        let secondUser: User = userBuilder.withName('name2').withNick('nick2').build();
        await userSQLRepository.save(user);
        await userSQLRepository.save(secondUser);
        await userSQLRepository.findAll().then(res=>expect(res).toHaveLength(2));
    })

})