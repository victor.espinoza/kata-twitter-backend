#!/bin/sh

docker_with_login() {
  echo "Login..."
  if [ ! -e ~/.docker/config.json ]; then
    command docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY || fail "Unable to login with docker"
  fi
  command docker "$@"
}

docker_build() {
  echo "Building docker image: $CI_REGISTRY_IMAGE"
  echo "Tagging images: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
  docker_with_login build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA .
}

publish_image() {
  echo $CI_REGISTRY
  docker_with_login push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
}
