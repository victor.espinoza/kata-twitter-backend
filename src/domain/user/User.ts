import UserBuilder from "./UserBuilder";
import {JsonProperty} from "json-object-mapper";
import "reflect-metadata";

export default class User {

    @JsonProperty({name: 'name', type:String})
    private _name :String;
    @JsonProperty({name:'nick', type:String})
    private _nick :String;

    constructor(param?: UserBuilder) {
        this._name = param ? param.getName() : '';
        this._nick = param ? param.getNick() : '';
    }

    getName(): String {
        return this._name;
    }

    setName(value: String) {
        this._name = value;
    }

    getNick(): String {
        return this._nick;
    }

    setNick(value: String) {
        this._nick = value;
    }

}