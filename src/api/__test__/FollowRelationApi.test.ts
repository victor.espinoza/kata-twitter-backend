import FollowRelationApi from "../FollowRelationApi";
import FollowRelationRepository from "../../domain/follow/FollowRelationRepository";
import FollowRelationService from "../../domain/follow/FollowRelationService";
import FollowRelationSqlRepository from "../../domain/follow/FollowRelationSqlRepository";
import SqlQuery from "../../infraestructure/database/SqlQuery";
import {configTest} from "../../infraestructure/database/Config";
import {request, response} from "express";

describe('FollowRelationApi',()=>{

    let followRelationRepository:FollowRelationRepository;
    let followRelationService:FollowRelationService;
    let followRelationApi:FollowRelationApi;

    beforeEach(()=>{
        followRelationRepository = new FollowRelationSqlRepository(new SqlQuery(configTest));
        followRelationService = new FollowRelationService(followRelationRepository);
        followRelationApi = new FollowRelationApi(followRelationService);
        response.send = jest.fn((message)=>message);
        response.end = jest.fn((message)=>message);
    })

    it('should add user to follow by nick',async()=>{
        followRelationService.addFollowing = jest.fn(()=>Promise.resolve());
        request.body = {nick: 'nick',nick_to_follow: 'nick_to_follow'};
        await followRelationApi.addFollowByNick(request, response)
            .then(()=>expectStatusAndMessage(200,'ok'));
    })

    it('should get all follows by nick',async()=>{
        let follows:String[] = ['follow'];
        followRelationService.getFollowings = jest.fn(() => Promise.resolve(follows));
        request.body = {nick: 'nick'};
        await followRelationApi.getFollowsByNick(request, response)
            .then(()=>expectStatusAndData(200,follows));
    })
})

function expectStatusAndMessage(status:number, message:String) {
    expect(response.statusCode).toEqual(status);
    expect(response.send).toBeCalledWith(message);
}
function expectStatusAndData(status:number, data:String[]) {
    expect(response.statusCode).toEqual(status);
    expect(response.end).toBeCalledWith(JSON.stringify(data));
}