import FollowRelation from "./FollowRelation";
import SqlQuery from "../../infraestructure/database/SqlQuery";
import FollowRelationRepository from "./FollowRelationRepository";
import {ObjectMapper} from "json-object-mapper";

export default class FollowRelationSqlRepository implements FollowRelationRepository{

    sqlQuery:SqlQuery;
    table:String;
    create:String = "CREATE TABLE IF NOT EXISTS follow_relation ( " +
        "id SERIAL PRIMARY KEY, " +
        "nick VARCHAR(100) NOT NULL, " +
        "nick_to_follow VARCHAR(100) NOT NULL " +
        ")"

    constructor(sqlQuery: SqlQuery) {
        this.sqlQuery = sqlQuery;
        this.table = "follow_relation";
        this.sqlQuery.createDb(this.create, []);
    }

    async findAllByNick(nick: String): Promise<FollowRelation[]> {
        let query: String = 'SELECT * FROM ' + this.table + ' WHERE nick = $1'
        let values: String[] = [nick];
        return await this.sqlQuery.select(query, values)
            .then((res: Object[]) => this._ArrayToPromise(res))
            .catch(() => this._ErrorPromise());
    }

    async save(followRelation: FollowRelation): Promise<any> {
        let queryText: String = 'INSERT INTO ' + this.table + ' (nick, nick_to_follow) values($1, $2)';
        let values: String[] = [followRelation.getUserNick(), followRelation.getUserToFollowNick()];
        return await this.sqlQuery.insert(queryText, values);
    }

    async findAll(): Promise<FollowRelation[]> {
        let query:String = 'SELECT * FROM ' + this.table;
        let values:String[] = [];

        return await this.sqlQuery.select(query,values)
            .then((res: Object[])=>this._ArrayToPromise(res))
            .catch(()=> this._ErrorPromise());
    }

    async deleteAll(): Promise<any> {
        return await this.sqlQuery.deleteAll(this.table);
    }

    private _ArrayToPromise(res: Object[]) :Promise<FollowRelation[]>{
        let followRelations:FollowRelation[] = [];
        for (let i=0;i<res.length;i++){
            followRelations.push(this.mapObjectToFollowRelation(res[i]));
        }
        return Promise.resolve(followRelations);
    }

    private mapObjectToFollowRelation(res: Object) :FollowRelation{
        return ObjectMapper.deserialize(FollowRelation, res);
    }

    private _ErrorPromise() :Promise<any>{
        return Promise.reject(null);
    }


}