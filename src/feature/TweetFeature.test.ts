import Tweet from "../domain/tweet/Tweet";
import TweetService from "../domain/tweet/TweetService";
import TweetSqlRepository from "../domain/tweet/TweetSqlRepository";
import TweetRepository from "../domain/tweet/TweetRepository";
import SqlQuery from "../infraestructure/database/SqlQuery";
import {configTest} from "../infraestructure/database/Config";

describe('TweetFeature',()=> {
    let tweet:Tweet;
    let secondTweet:Tweet;
    let tweetRepository:TweetRepository;
    let tweetService:TweetService;
    let sqlQuery:SqlQuery;

    beforeEach(()=>{
        sqlQuery = new SqlQuery(configTest);
        tweetRepository = new TweetSqlRepository(sqlQuery);
        tweetService = new TweetService(tweetRepository);
        tweet = new Tweet("nick","tweet");
        secondTweet = new Tweet("nick","this is my second tweet");
    })
    afterEach(async ()=>{
        await tweetService.deleteAll();
    })

    it('should save a users tweet',async ()=>{
        await tweetService.addTweet(tweet);
        await tweetService.addTweet(secondTweet);
        await tweetService.findAllTweets()
            .then(res=>expect(res).toHaveLength(2));
    })

    it('should get tweets by user nick', async()=>{
        await tweetService.addTweet(tweet);
        await tweetService.addTweet(secondTweet);
        await tweetService.findAllTweetsByNick("nick")
            .then(res=>{
                expect(res).toContainEqual(tweet.getTweet());
                expect(res).toContainEqual(secondTweet.getTweet());
            })
    })
})