import User from "./User";
import UserRepository from "./UserRepository";

export default class UserService {
    private userRepository: UserRepository;

    constructor(userRepository:UserRepository) {
        this.userRepository = userRepository;
    }

    async register(user: User):Promise<void>{
        let userFound:User|null = null;
        try{
             userFound = await this.userRepository.findByNick(user.getNick())
                .then(res=>res)
        }catch (e) {
            console.log(e);
        }
        return userFound ? Promise.reject('Nickname Already Exist') :
            Promise.resolve(this.userRepository.save(user));
    }

    async updateName(user: User): Promise<void> {
        return await this.userRepository.update(user);
    }

    async findAll(): Promise<User[]> {
        return await this.userRepository.findAll();
    }

    async findByNick(nick: String): Promise<User | null> {
        return await this.userRepository.findByNick(nick);
    }
}