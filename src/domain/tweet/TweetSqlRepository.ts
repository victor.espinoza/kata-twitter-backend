import TweetRepository from "./TweetRepository";
import Tweet from "./Tweet";
import SqlQuery from "../../infraestructure/database/SqlQuery";
import {ObjectMapper} from "json-object-mapper";

export default class TweetSqlRepository implements TweetRepository{
    private sqlQuery: SqlQuery;
    private readonly table: string;
    create:String = "CREATE TABLE IF NOT EXISTS tweet( " +
        "id SERIAL PRIMARY KEY, " +
        "nick VARCHAR(100) NOT NULL, " +
        "tweet VARCHAR(150) NOT NULL " +
        ")"

    constructor(sqlQuery:SqlQuery) {
        this.sqlQuery = sqlQuery;
        this.table = "tweet";
        this.sqlQuery.createDb(this.create,[]);
    }

    async findAllByNick(nick: string): Promise<Tweet[]> {
        let query:String = 'SELECT * FROM ' + this.table +" WHERE nick = $1";
        let values:String[] = [nick];
        return await this.sqlQuery.select(query, values)
            .then(res=>Promise.resolve(this._ArrayToPromise(res)));
    }

    async save(tweet: Tweet): Promise<any> {
        let queryText: String = 'INSERT INTO ' + this.table + ' (nick, tweet) values($1, $2)';
        let values: String[] = [tweet.getNick(), tweet.getTweet()];
        return await this.sqlQuery.insert(queryText, values);
    }

    async deleteAll(): Promise<any> {
        return await this.sqlQuery.deleteAll(this.table);
    }

    async findAll(): Promise<Tweet[]> {
        let query:String = 'SELECT * FROM ' + this.table;
        let values:String[] = [];
        return await this.sqlQuery.select(query, values)
            .then(res=>Promise.resolve(this._ArrayToPromise(res)))
    }

    private _ArrayToPromise(res: Object[]) :Promise<Tweet[]>{
        let tweets:Tweet[] = [];
        for (let i=0;i<res.length;i++){
            tweets.push(this.mapObjectToTweet(res[i]));
        }
        return Promise.resolve(tweets);
    }

    private mapObjectToTweet(res: Object) :Tweet{
        return ObjectMapper.deserialize(Tweet, res);
    }

}