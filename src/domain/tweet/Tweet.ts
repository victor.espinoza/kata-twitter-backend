import "reflect-metadata";
import {JsonProperty} from "json-object-mapper";
export default class Tweet{
    @JsonProperty({name: 'nick', type:String})
    private readonly _nick:String;
    @JsonProperty({name: 'tweet', type:String})
    private readonly _tweet:String;

    constructor(nick?:String, tweet?:String) {
        this._nick = nick ? nick : '';
        this._tweet = tweet ? tweet : '';
    }

    getNick(): String {
        return this._nick;
    }

    getTweet(): String {
        return this._tweet;
    }
}