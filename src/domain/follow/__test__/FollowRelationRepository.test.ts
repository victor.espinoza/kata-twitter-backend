import FollowRelation from "../FollowRelation";
import FollowRelationRepository from "../FollowRelationRepository";
import FollowRelationSqlRepository from "../FollowRelationSqlRepository";
import SqlQuery from "../../../infraestructure/database/SqlQuery";
import {configTest} from "../../../infraestructure/database/Config";

describe.skip('FollowRelationDiskRepository',()=>{
    let followRelationRepository:FollowRelationRepository;
    let followRelation:FollowRelation;
    let sqlQuery:SqlQuery;

    beforeEach(()=>{
        sqlQuery = new SqlQuery(configTest);
        followRelationRepository = new FollowRelationSqlRepository(sqlQuery);
        followRelation = new FollowRelation('nick','toFollow');
    })

    afterEach(async ()=>{
        await followRelationRepository.deleteAll();
    })

    it('should save expected number of follow relations',async ()=>{
        let secondFollowRelation: FollowRelation = new FollowRelation('nick2','toFollow2');
        await followRelationRepository.save(followRelation);
        await followRelationRepository.save(secondFollowRelation);
        await followRelationRepository.findAll().then(res=>expect(res).toHaveLength(2));
    })

    it('should find a follow relation by nick',async ()=>{
        let followsReturned:FollowRelation[];
        await followRelationRepository.save(followRelation);
        followsReturned = await followRelationRepository.findAllByNick(followRelation.getUserNick());
        expect(followsReturned).toContainEqual(followRelation);
    })
})