import FollowRelationService from "../domain/follow/FollowRelationService";

export default class FollowRelationApi{

    private followRelationService:FollowRelationService;

    constructor(followRelationService: FollowRelationService) {
        this.followRelationService = followRelationService;
    }

    async addFollowByNick(request: any, response: any) {
        let {nick, nick_to_follow} = request.body;
        if(!nick || !nick_to_follow)
            this.responseStatusAndMessage(response,400,"bad request")
        else
            await this.followRelationService.addFollowing(nick,nick_to_follow)
            .then(() => this.responseStatusAndMessage(response,200,'ok'))
            .catch(reason => this.responseStatusAndMessage(response,400,reason));
    }

    async getFollowsByNick(request: any, response: any) {
        let nick:String = request.body.nick;
        if(!nick)
            this.responseStatusAndMessage(response,400,"bad request")
        else
            this.followRelationService.getFollowings(nick)
            .then((follows:String[])=> this.responseStatusAndData(response,200,follows))
            .catch(reason => this.responseStatusAndMessage(response,400,reason));
    }

    private responseStatusAndMessage(res:any, status:number, message:String) {
        res.status(status);
        res.send(message);
    }
    private responseStatusAndData(res:any, status:number, data:String[]) {
        res.status(status);
        res.end(JSON.stringify(data));
    }

}