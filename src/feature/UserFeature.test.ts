import UserService from "../domain/user/UserService";
import UserMemoryRepository from "../domain/user/UserMemoryRepository";
import User from "../domain/user/User";
import UserBuilder from "../domain/user/UserBuilder";
import UserSQLRepository from "../domain/user/UserSQLRepository";
import SqlQuery from "../infraestructure/database/SqlQuery";
import {configTest} from "../infraestructure/database/Config";

describe('UserFeature',()=> {
    let userService: UserService;
    let user:User;
    let userUpdate:User;
    let userBuilder:UserBuilder;
    let userRepository: UserSQLRepository;

    beforeEach(()=>{
        userRepository = new UserSQLRepository(new SqlQuery(configTest));
        userService = new UserService(userRepository);
        userBuilder = new UserBuilder();
        user = userBuilder.withNick('nick').withName('name').build();
        userUpdate = userBuilder.withNick('nick').withName('name update').build();
    })

    afterEach(async ()=>{
        await userRepository.deleteAll();
    })

    it('should register a user',async ()=>{
        let userRegistered:User|null;
        await userService.register(user);
        userRegistered = await userService.findByNick(user.getNick())
            .then(res =>res);
        expect(userRegistered).toBeDefined();
        expect(userRegistered?.getNick()).toBe(user.getNick());
    })

    it('should throw error if a user already exist',async ()=>{
        await userService.register(user);
        await userService.register(user).catch(reason => expect(reason).toEqual("Nickname Already Exist"))
    })

    it('should update real name of user',async ()=>{

        let userUpdated:User|null;
        await userService.register(user);
        await userService.updateName(userUpdate);
        userUpdated = await userService.findByNick(user.getNick())
            .then((res)=>res);
        expect(userUpdated).toBeDefined();
        expect(userUpdated?.getName()).toBe(userUpdate.getName());
    })

})