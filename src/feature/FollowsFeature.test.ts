import FollowRelationService from "../domain/follow/FollowRelationService";
import FollowRelationRepository from "../domain/follow/FollowRelationRepository";
import SqlQuery from "../infraestructure/database/SqlQuery";
import {configTest} from "../infraestructure/database/Config";
import FollowRelationSqlRepository from "../domain/follow/FollowRelationSqlRepository";

describe('FollowsFeature',()=> {
    let followRelationService :FollowRelationService;
    let followRelationRepository : FollowRelationRepository;
    let sqlQuery:SqlQuery;
    let NICK :String = 'nick';

    beforeEach(()=>{
        sqlQuery = new SqlQuery(configTest);
        followRelationRepository = new FollowRelationSqlRepository(sqlQuery);
        followRelationService = new FollowRelationService(followRelationRepository);
    })

    afterEach(async ()=>{
        await followRelationRepository.deleteAll();
    })

    it('should add a follow relation',async ()=>{
        let follows:String[];
        await followRelationService.addFollowing(NICK, 'nickToFollow');
        follows = await followRelationService.getFollowings(NICK);
        expect(follows).toHaveLength(1);
    })

    it('should add and get follows of a user by nick',async ()=>{
        let follows:String[];
        const nickToFollow1 = 'follow1';
        const nickToFollow2 = 'follow2';
        await followRelationService.addFollowing(NICK, nickToFollow1);
        await followRelationService.addFollowing(NICK, nickToFollow2);
        follows = await followRelationService.getFollowings(NICK);
        expect(follows).toHaveLength(2);
        expect(follows).toContain(nickToFollow1);
        expect(follows).toContain(nickToFollow2);
    })
})