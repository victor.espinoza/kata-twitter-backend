#!/bin/sh

log_into_acceso_d() {
  apk add --no-cache git bash curl
  git clone --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/etermax/sre/acceso-d.git && bash acceso-d/client/authorize.sh $@
}

prepare_ssh_config() {
  echo "Setting ~/.ssh/id_rsa"
  mkdir $HOME/.ssh/
  echo "${SSH_PRIVATE_KEY}" >~/.ssh/id_rsa
  chmod 600 ~/.ssh/id_rsa

  echo "${SSH_PRIVATE_KEY}" >>etermax.key
  chmod 600 etermax.key
  apk add --no-cache openssh-client
}

run_with_ssh() {
  ssh -i etermax.key -o StrictHostKeyChecking=no $@
}

login_docker_in_host() {
	local host=$1
	echo "Login..."
  run_with_ssh etermax@"${host}" docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} $CI_REGISTRY || fail "Unable to login with docker"
}

rsync_docker_compose() {
  prepare_ssh_config
  local host=$1
  local compose_file=$2

  apk add --no-cache rsync
  rsync -avz --delete --progress -e "ssh -i etermax.key -o StrictHostKeyChecking=no" $compose_file etermax@"${host}":/home/etermax/
}

deploy_compose() {
  local host=$1
  local compose_file=$2
  local tag=$3
  login_docker_in_host $host
  echo "Runing docker compose"
  echo "export RELEASE_TAG=$tag && docker-compose -f $compose_file up -d"
  run_with_ssh etermax@"${host}" "export RELEASE_TAG=${tag} && docker-compose -f ${compose_file} up -d"
}
