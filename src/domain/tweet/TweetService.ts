import TweetRepository from "./TweetRepository";
import Tweet from "./Tweet";

export default class TweetService{
    private tweetRepository: TweetRepository;

    constructor(tweetRepository: TweetRepository) {
        this.tweetRepository = tweetRepository;
    }


    async deleteAll() {
        return await this.tweetRepository.deleteAll();
    }

    async addTweet(tweet: Tweet) {
        return await this.tweetRepository.save(tweet);
    }

    async findAllTweets() {
        return await this.tweetRepository.findAll();
    }

    async findAllTweetsByNick(nick: string) {
        let tweets:Tweet[] = await this.tweetRepository.findAllByNick(nick).then(tweets=>tweets);
        return Promise.resolve(tweets.map(tweet=> tweet.getTweet()));
    }
}