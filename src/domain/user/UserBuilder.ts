import User from "./User";


export default class UserBuilder {
    private _name ?:String;
    private _nick ?:String;

    withName(name:String):UserBuilder{
        this._name = name;
        return this;
    }

    withNick(nick:String):UserBuilder{
        this._nick = nick;
        return this;
    }

    build():User{
        return new User(this);
    }

    getName(): String {
        return <String>this._name;
    }

    getNick(): String {
        return <String>this._nick;
    }
}