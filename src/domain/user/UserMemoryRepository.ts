import UserRepository from "./UserRepository";
import User from "./User";

export default class UserMemoryRepository implements UserRepository{

    private map:Map<String, User>;

    constructor() {
        this.map = new Map();
    }

    async save(user: User): Promise<any> {
        await this.map.set(user.getNick(), user);
        return await new Promise<any>(resolve => resolve(user));
    }

    async findByNick(nick: String): Promise<User | null> {
        return await new Promise(resolve => resolve(this.map.get(nick)));
    }

    async findAll(): Promise<User[]> {
        return await new Promise<User[]>(resolve => resolve(Array.from(this.map.values())));
    }

    deleteAll(): Promise<any> {
        return Promise.resolve(undefined);
    }

    update(user: User): Promise<any> {
        return Promise.resolve(undefined);
    }

}