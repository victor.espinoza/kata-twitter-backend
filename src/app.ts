import UserSQLRepository from "./domain/user/UserSQLRepository";
import SqlQuery from "./infraestructure/database/SqlQuery";
import {configProd} from "./infraestructure/database/Config";
import UserService from "./domain/user/UserService";
import UserApi from "./api/UserApi";
import express from "express";
const cors = require('cors');
import bodyParser from "body-parser";
import FollowRelationApi from "./api/FollowRelationApi";
import FollowRelationSqlRepository from "./domain/follow/FollowRelationSqlRepository";
import FollowRelationService from "./domain/follow/FollowRelationService";
import TweetSqlRepository from "./domain/tweet/TweetSqlRepository";
import TweetApi from "./api/TweetApi";
import TweetService from "./domain/tweet/TweetService";
const app: express.Application = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const sqlQuery = new SqlQuery(configProd);
const userRepository = new UserSQLRepository(sqlQuery);
const userService = new UserService(userRepository);
const userApi = new UserApi(userService);
const followRepository = new FollowRelationSqlRepository(sqlQuery);
const followRelationService = new FollowRelationService(followRepository)
const followRelationApi = new FollowRelationApi(followRelationService);
const tweetRepository = new TweetSqlRepository(sqlQuery);
const tweetApi = new TweetApi(new TweetService(tweetRepository));

app.post('/user/register', (request,res) =>{
    return userApi.register(request,res);
});

app.post('/user/change-name', (request,res) =>{
    return userApi.updateName(request,res);
});

app.post('/follows/add-follow',(request, res)=>{
    return followRelationApi.addFollowByNick(request, res);
});

app.post('/follows/get-follows',(request, res)=>{
    return followRelationApi.getFollowsByNick(request, res);
})

app.post('/tweet/add-tweet',(request, res)=>{
    return tweetApi.addTweet(request, res);
});

app.post('/tweet/get-tweets',(request, res)=>{
    return tweetApi.getTweetsByNick(request, res);
})

app.listen(3000, function () {
    console.log('App is listening on port 3000!');
});
