import FollowRelation from "../FollowRelation";
import FollowRelationRepository from "../FollowRelationRepository";
import FollowRelationMemoryRepository from "../FollowRelationMemoryRepository";

describe.skip('FollowRelationDiskRepository',()=>{
    let followRelationRepository:FollowRelationRepository;
    let followRelation:FollowRelation;

    beforeEach(()=>{
        followRelationRepository = new FollowRelationMemoryRepository();
        followRelation = new FollowRelation('nick','toFollow');
    })


    it('should storage expected number of follow relations',async ()=>{
        let secondFollowRelation: FollowRelation = new FollowRelation('nick2','toFollow2');
        await followRelationRepository.save(followRelation);
        await followRelationRepository.save(secondFollowRelation);
        await followRelationRepository.findAll().then(res=>expect(res).toHaveLength(2));
    })

    it('should find a follow relation by nick',async ()=>{
        let followsReturned:FollowRelation[];
        await followRelationRepository.save(followRelation);
        followsReturned = await followRelationRepository.findAllByNick(followRelation.getUserNick());
        expect(followsReturned).toContainEqual(followRelation);
    })
})