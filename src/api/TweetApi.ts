import TweetService from "../domain/tweet/TweetService";
import Tweet from "../domain/tweet/Tweet";

export default class TweetApi{
    private tweetService:TweetService;

    constructor(tweetService: TweetService) {
        this.tweetService = tweetService;
    }

    async addTweet(request: any, response: any){
        let tweet:Tweet = new Tweet(request.body.nick,request.body.tweet);
        await this.tweetService.addTweet(tweet)
            .then(() => this.responseStatusAndMessage(response,200,'ok'))
            .catch(reason => this.responseStatusAndMessage(response,400,reason));
    }

    async getTweetsByNick(request: any, response: any){
        await this.tweetService.findAllTweetsByNick(request.body.nick)
            .then((tweets:String[])=> this.responseStatusAndData(response,200,tweets))
            .catch(reason => this.responseStatusAndMessage(response,400,reason));
    }

    private responseStatusAndMessage(res:any, status:number, message:String) {
        res.status(status);
        res.send(message);
    }
    private responseStatusAndData(res:any, status:number, data:String[]) {
        res.status(status);
        res.end(JSON.stringify(data));
    }
}