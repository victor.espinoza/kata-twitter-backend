import express from "express";
import bodyParser from "body-parser";
import UserSQLRepository from "../../domain/user/UserSQLRepository";
import SqlQuery from "../database/SqlQuery";
import {configProd, configTest} from "../database/Config";
import UserService from "../../domain/user/UserService";
import UserApi from "../../api/UserApi";
const cors = require('cors');
const app: express.Application = express();

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const userRepository = new UserSQLRepository(new SqlQuery(configProd));
const userService = new UserService(userRepository);
const userApi = new UserApi(userService);

app.post('/user/register', function(request,res) {
    return userApi.register(request,res);
});

app.listen(3000, function () {
    console.log('App is listening on port 3000!');
});

export default app;