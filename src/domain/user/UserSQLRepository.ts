import UserRepository from "./UserRepository";
import User from "./User";
import {ObjectMapper} from "json-object-mapper";
import SqlQuery from "../../infraestructure/database/SqlQuery";

export default class UserSQLRepository implements UserRepository{
    sqlQuery:SqlQuery;
    table:String;
    create:String = "CREATE TABLE IF NOT EXISTS user_tw ( " +
        "id SERIAL PRIMARY KEY, " +
        "nick VARCHAR(100) NOT NULL, " +
        "name VARCHAR(100) NOT NULL " +
        ")"

    constructor(sqlQuery:SqlQuery) {
        this.sqlQuery = sqlQuery;
        this.table = "user_tw";
        this.sqlQuery.createDb(this.create, []);
    }

    async save(user: User): Promise<any> {
        let queryText: String = 'INSERT INTO ' + this.table + ' (nick, name) values($1, $2)'
        let values: String[] = [user.getNick(), user.getName()];
        return await this.sqlQuery.insert(queryText, values);
    }

    async deleteAll(): Promise<any> {
        return await this.sqlQuery.deleteAll(this.table);
    }

    async update(user:User): Promise<any> {
        let values: String[] = [user.getName(), user.getNick()];
        return await this.sqlQuery.update(this.table,values);
    }

    async findAll(): Promise<User[]> {
        let query:String = 'SELECT * FROM ' + this.table;
        let values:String[] = [];

        return await this.sqlQuery.select(query,values)
            .then((res: Object[])=>this._ArrayToUserPromise(res))
            .catch(()=> this._ErrorPromise());
    }

    async findByNick(nick: String): Promise<User|null> {
        let query:String = 'SELECT * FROM ' + this.table + ' WHERE nick = $1'
        let values: String[] = [nick];
        return await this.sqlQuery.selectFirst(query, values)
            .then((res: Object|null)=>this._ObjectToUserPromise(res))
            .catch(()=> this._ErrorPromise());
    }

    private _ErrorPromise() :Promise<any>{
        return Promise.reject(null);
    }

    private _ObjectToUserPromise(res: Object|null) :Promise<User|null>{
        let user = res ? this.mapJsonObjectToUser(res) : null;
        return Promise.resolve(user);
    }

    private mapJsonObjectToUser(res: Object) :User{
        return ObjectMapper.deserialize(User, res);
    }

    private _ArrayToUserPromise(res: Object[]) :Promise<User[]>{
        let users:User[] = [];
        for (let i=0;i<res.length;i++){
            users.push(this.mapJsonObjectToUser(res[i]));
        }
        return Promise.resolve(users);
    }


}