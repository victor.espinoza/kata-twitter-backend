import Tweet from "./Tweet";

export default interface TweetRepository{

    save(tweet:Tweet):Promise<any>;
    deleteAll():Promise<any>;
    findAll(): Promise<Tweet[]>;
    findAllByNick(nick: string): Promise<any>;
}