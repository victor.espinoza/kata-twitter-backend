import UserApi from "../UserApi";
import UserRepository from "../../domain/user/UserRepository";
import UserService from "../../domain/user/UserService";
import UserSQLRepository from "../../domain/user/UserSQLRepository";
import SqlQuery from "../../infraestructure/database/SqlQuery";
import {configTest} from "../../infraestructure/database/Config";
import {request, response} from "express";

function expectStatusAndMessage(status:number, message:String) {
    expect(response.statusCode).toEqual(status);
    expect(response.send).toBeCalledWith(message);
}

describe("User Api", () => {
    let userApi:UserApi;
    let userRepository:UserRepository;
    let userService:UserService;
    beforeEach(()=>{
        userRepository = new UserSQLRepository(new SqlQuery(configTest));
        userService = new UserService(userRepository);
        userApi = new UserApi(userService);
        response.send = jest.fn((message)=>message);
    })

    it("should response status 400 and send message error if user already exist", async(done) => {
        userService.register = jest.fn(()=>Promise.reject('user already exist'));
        request.body = {nick: 'nickname',name: 'name'};
        await userApi.register(request, response)
            .then(()=>expectStatusAndMessage(400,'user already exist'));
        done();
    })

    it("should register and return status 200",async()=>{
        userService.register = jest.fn(()=>Promise.resolve());
        request.body = {nick: 'nickname',name: 'name'};
        await userApi.register(request,response)
            .then(()=>expectStatusAndMessage(200,'ok'));
    })

    it('should update user name',async()=>{
        userService.updateName = jest.fn(()=>Promise.resolve());
        request.body = {nick: 'nickname',name: 'nameUpdate'};
        await userApi.updateName(request, response)
            .then(()=>expectStatusAndMessage(200,'ok'));
    })


})