import UserMemoryRepository from "../UserMemoryRepository";
import User from "../User";
import UserBuilder from "../UserBuilder";

describe.skip('UserMemoryRepository',()=>{
    let userRepository:UserMemoryRepository;
    let userBuilder:UserBuilder;
    let user:User;

    beforeEach(()=>{
        userRepository = new UserMemoryRepository();
        userBuilder = new UserBuilder();
        user = userBuilder.withName('name').withNick('nick').build();
    })

    it('should save expected number of users',async (done)=>{
        let secondUser: User = userBuilder.withName('name2').withNick('nick2').build();
        let thirthUser: User = userBuilder.withName('name3').withNick('nick3').build();
        await userRepository.save(user);
        await userRepository.save(secondUser);
        await userRepository.save(thirthUser);
        await userRepository.findAll().then(res=>expect(res).toHaveLength(3));
        done();
    })

    it('should find a saved user',async (done)=>{
        await userRepository.save(user);
        await userRepository.findByNick(user.getNick())
            .then(res=>expect(res).toBe(user));
        done();
    })

})