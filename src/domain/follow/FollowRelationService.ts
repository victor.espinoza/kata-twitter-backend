import FollowRelationRepository from "./FollowRelationRepository";
import FollowRelation from "./FollowRelation";

export default class FollowRelationService {
    private followRelationRepository: FollowRelationRepository;

    constructor(followRelationRepository: FollowRelationRepository) {
        this.followRelationRepository = followRelationRepository;
    }

    async addFollowing(nick: String, nickToFollow: String) {
        let follows: FollowRelation[] = await this.followRelationRepository.findAllByNick(nickToFollow).
            then(followings => followings).catch(()=>[]);
        if(!follows)
            return Promise.reject("Nick not found")
        return await this.followRelationRepository.save(new FollowRelation(nick, nickToFollow));
    }

    async getFollowings(nick: String): Promise<String[]> {
        let followRelations: FollowRelation[] = await this.followRelationRepository
            .findAllByNick(nick).then(follows=>follows);
        if(!followRelations)
            return Promise.reject("Nick not found")
        return Promise.resolve(followRelations.map(relation => relation.getUserToFollowNick()));
    }
}