import "reflect-metadata";
import {JsonProperty} from "json-object-mapper";

export default class FollowRelation{
    @JsonProperty({name: 'nick', type:String})
    private readonly nick:String;
    @JsonProperty({name: 'nick_to_follow', type:String})
    private readonly nickToFollow:String;

    constructor(userNick?: String, userToFollowNick?: String) {
        this.nick = userNick ? userNick : '';
        this.nickToFollow = userToFollowNick ? userToFollowNick : '';
    }

    getUserNick(): String {
        return this.nick;
    }

    getUserToFollowNick(): String {
        return this.nickToFollow;
    }
}