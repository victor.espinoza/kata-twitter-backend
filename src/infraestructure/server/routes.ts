import { Router } from 'express';
import UserApi from "../../api/UserApi";
import UserService from "../../domain/user/UserService";
import UserSQLRepository from "../../domain/user/UserSQLRepository";
import SqlQuery from "../database/SqlQuery";
import {configTest} from "../database/Config";

const routes = Router();


export default routes;
