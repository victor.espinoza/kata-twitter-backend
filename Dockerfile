FROM node:10.16-alpine
#RUN apk add bash

WORKDIR /twitter-back
COPY . .
RUN npm i
RUN npm build
CMD ["npm", "start"]
