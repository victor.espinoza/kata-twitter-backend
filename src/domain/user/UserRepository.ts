import User from "./User";

export default interface UserRepository {


    save(user:User):Promise<any>;

    findByNick(nick: String):Promise<User | null>;

    findAll(): Promise<User[]>;

    deleteAll():Promise<any>;

    update(user:User):Promise<any>;
}