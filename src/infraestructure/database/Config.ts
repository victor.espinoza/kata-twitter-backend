
export const configProd = {
    user: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    database: 'twit',
    password: 'password',
    port: 5432,
    ssl: false
};
export const configTest = {
    user: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    database: 'twit_test',
    password: 'password',
    port: 5432,
    ssl: false
};
