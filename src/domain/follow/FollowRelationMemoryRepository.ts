import FollowRelationRepository from "./FollowRelationRepository";
import FollowRelation from "./FollowRelation";

export default class FollowRelationMemoryRepository implements FollowRelationRepository{

    private followRelations:FollowRelation[];

    constructor() {
        this.followRelations = [];
    }

    save(followRelation:FollowRelation):Promise<any>{
        this.followRelations.push(followRelation);
        return Promise.resolve();
    }

    findAllByNick(nick: String): Promise<FollowRelation[]> {
        let follows:FollowRelation[] = this.followRelations.filter(follow => follow.getUserNick() === nick);
        return Promise.resolve(follows);
    }

    deleteAll(): Promise<any> {
        this.followRelations = [];
        return Promise.resolve();
    }

    findAll(): Promise<FollowRelation[]> {
        return Promise.resolve(this.followRelations);
    }
}